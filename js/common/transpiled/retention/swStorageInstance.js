"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by sekot on 2016-10-27.
 */

/**
 * Entity that knows how to perform read write opperations to storage
 * @param String key name of storage
 * @param StorageArea storage
 * @param Event onChanged
 * @constructor
 */
var swStorageInstance = function () {
  function swStorageInstance(key, storage) {
    _classCallCheck(this, swStorageInstance);

    this.key = key;
    this.storage = storage;
  }

  /**
   * Gets the value stored.
   * @returns {Promise}
   */


  _createClass(swStorageInstance, [{
    key: "get",
    value: function get() {
      var self = this;
      return new Promise(function (resolve) {
        self.storage.get(self.key, function (value) {
          value = value[self.key];
          self.lastAnswer = value;
          resolve(value);
        });
      });
    }

    /**
     * Sets the new value
     * @param Onject value new value
     * @returns {Promise}
     */

  }, {
    key: "set",
    value: function set(value) {
      var self = this;
      return new Promise(function (resolve) {
        var obj = {};
        obj[self.key] = value;
        self.storage.set(obj, resolve);
      });
    }

    /**
     * Gets current value, compares it with new and if different updates it
     * @param Onject value new value
     * @returns {Promise}
     */

  }, {
    key: "setIfNew",
    value: function setIfNew(value) {
      var self = this;
      return new Promise(function (resolve) {
        self.get().then(function (v) {
          if (v == value) {
            resolve();
          } else {
            self.set(value).then(resolve);
          }
        });
      });
    }

    /**
     * Gets the value, updates it as updateFunction requires it
     * and sets the updated value
     * @param Function updateFunction function(value){return updatedValue;}
     * @returns {Promise}
     */

  }, {
    key: "update",
    value: function update(updateFunction) {
      if (typeof updateFunction !== "function") {
        throw new Error("Illegal invocation. Function expected.");
      }
      var self = this;
      return new Promise(function (resolve) {
        self.get().then(function (value) {
          var updatedValue = updateFunction(value);
          self.setIfNew(updatedValue).then(resolve);
        });
      });
    }
  }]);

  return swStorageInstance;
}();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJldGVudGlvbi9zd1N0b3JhZ2VJbnN0YW5jZS5qcyJdLCJuYW1lcyI6WyJzd1N0b3JhZ2VJbnN0YW5jZSIsImtleSIsInN0b3JhZ2UiLCJzZWxmIiwiUHJvbWlzZSIsImdldCIsInZhbHVlIiwibGFzdEFuc3dlciIsInJlc29sdmUiLCJvYmoiLCJzZXQiLCJ0aGVuIiwidiIsInVwZGF0ZUZ1bmN0aW9uIiwiRXJyb3IiLCJ1cGRhdGVkVmFsdWUiLCJzZXRJZk5ldyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUE7Ozs7QUFJQTs7Ozs7OztJQU9NQSxpQjtBQUNKLDZCQUFZQyxHQUFaLEVBQWlCQyxPQUFqQixFQUF5QjtBQUFBOztBQUN2QixTQUFLRCxHQUFMLEdBQVdBLEdBQVg7QUFDQSxTQUFLQyxPQUFMLEdBQWVBLE9BQWY7QUFDRDs7QUFFRDs7Ozs7Ozs7MEJBSUs7QUFDSCxVQUFNQyxPQUFPLElBQWI7QUFDQSxhQUFPLElBQUlDLE9BQUosQ0FBWSxtQkFBVTtBQUMzQkQsYUFBS0QsT0FBTCxDQUFhRyxHQUFiLENBQWlCRixLQUFLRixHQUF0QixFQUEyQixpQkFBUTtBQUNqQ0ssa0JBQVFBLE1BQU1ILEtBQUtGLEdBQVgsQ0FBUjtBQUNBRSxlQUFLSSxVQUFMLEdBQWtCRCxLQUFsQjtBQUNBRSxrQkFBUUYsS0FBUjtBQUNELFNBSkQ7QUFLRCxPQU5NLENBQVA7QUFPRDs7QUFFRDs7Ozs7Ozs7d0JBS0lBLEssRUFBTTtBQUNSLFVBQU1ILE9BQU8sSUFBYjtBQUNBLGFBQU8sSUFBSUMsT0FBSixDQUFZLG1CQUFVO0FBQzNCLFlBQU1LLE1BQU0sRUFBWjtBQUNBQSxZQUFJTixLQUFLRixHQUFULElBQWdCSyxLQUFoQjtBQUNBSCxhQUFLRCxPQUFMLENBQWFRLEdBQWIsQ0FBaUJELEdBQWpCLEVBQXNCRCxPQUF0QjtBQUNELE9BSk0sQ0FBUDtBQUtEOztBQUVEOzs7Ozs7Ozs2QkFLU0YsSyxFQUFNO0FBQ2IsVUFBTUgsT0FBTyxJQUFiO0FBQ0EsYUFBTyxJQUFJQyxPQUFKLENBQVksbUJBQVU7QUFDM0JELGFBQUtFLEdBQUwsR0FBV00sSUFBWCxDQUFnQixhQUFJO0FBQ2xCLGNBQUlDLEtBQUtOLEtBQVQsRUFBZ0I7QUFDZEU7QUFDRCxXQUZELE1BRU87QUFDTEwsaUJBQUtPLEdBQUwsQ0FBU0osS0FBVCxFQUFnQkssSUFBaEIsQ0FBcUJILE9BQXJCO0FBQ0Q7QUFDRixTQU5EO0FBT0QsT0FSTSxDQUFQO0FBU0Q7O0FBRUQ7Ozs7Ozs7OzsyQkFNT0ssYyxFQUFlO0FBQ3BCLFVBQUksT0FBT0EsY0FBUCxLQUEwQixVQUE5QixFQUEwQztBQUN4QyxjQUFNLElBQUlDLEtBQUosQ0FBVSx3Q0FBVixDQUFOO0FBQ0Q7QUFDRCxVQUFNWCxPQUFPLElBQWI7QUFDQSxhQUFPLElBQUlDLE9BQUosQ0FBWSxtQkFBVTtBQUMzQkQsYUFBS0UsR0FBTCxHQUFXTSxJQUFYLENBQWdCLGlCQUFRO0FBQ3RCLGNBQU1JLGVBQWVGLGVBQWVQLEtBQWYsQ0FBckI7QUFDQUgsZUFBS2EsUUFBTCxDQUFjRCxZQUFkLEVBQTRCSixJQUE1QixDQUFpQ0gsT0FBakM7QUFDRCxTQUhEO0FBSUQsT0FMTSxDQUFQO0FBTUQiLCJmaWxlIjoicmV0ZW50aW9uL3N3U3RvcmFnZUluc3RhbmNlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqXHJcbiAqIENyZWF0ZWQgYnkgc2Vrb3Qgb24gMjAxNi0xMC0yNy5cclxuICovXHJcblxyXG4vKipcclxuICogRW50aXR5IHRoYXQga25vd3MgaG93IHRvIHBlcmZvcm0gcmVhZCB3cml0ZSBvcHBlcmF0aW9ucyB0byBzdG9yYWdlXHJcbiAqIEBwYXJhbSBTdHJpbmcga2V5IG5hbWUgb2Ygc3RvcmFnZVxyXG4gKiBAcGFyYW0gU3RvcmFnZUFyZWEgc3RvcmFnZVxyXG4gKiBAcGFyYW0gRXZlbnQgb25DaGFuZ2VkXHJcbiAqIEBjb25zdHJ1Y3RvclxyXG4gKi9cclxuY2xhc3Mgc3dTdG9yYWdlSW5zdGFuY2Uge1xyXG4gIGNvbnN0cnVjdG9yKGtleSwgc3RvcmFnZSl7XHJcbiAgICB0aGlzLmtleSA9IGtleTtcclxuICAgIHRoaXMuc3RvcmFnZSA9IHN0b3JhZ2U7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXRzIHRoZSB2YWx1ZSBzdG9yZWQuXHJcbiAgICogQHJldHVybnMge1Byb21pc2V9XHJcbiAgICovXHJcbiAgZ2V0KCl7XHJcbiAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+e1xyXG4gICAgICBzZWxmLnN0b3JhZ2UuZ2V0KHNlbGYua2V5LCB2YWx1ZSA9PntcclxuICAgICAgICB2YWx1ZSA9IHZhbHVlW3NlbGYua2V5XTtcclxuICAgICAgICBzZWxmLmxhc3RBbnN3ZXIgPSB2YWx1ZTtcclxuICAgICAgICByZXNvbHZlKHZhbHVlKTtcclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldHMgdGhlIG5ldyB2YWx1ZVxyXG4gICAqIEBwYXJhbSBPbmplY3QgdmFsdWUgbmV3IHZhbHVlXHJcbiAgICogQHJldHVybnMge1Byb21pc2V9XHJcbiAgICovXHJcbiAgc2V0KHZhbHVlKXtcclxuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKHJlc29sdmUgPT57XHJcbiAgICAgIGNvbnN0IG9iaiA9IHt9O1xyXG4gICAgICBvYmpbc2VsZi5rZXldID0gdmFsdWU7XHJcbiAgICAgIHNlbGYuc3RvcmFnZS5zZXQob2JqLCByZXNvbHZlKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0cyBjdXJyZW50IHZhbHVlLCBjb21wYXJlcyBpdCB3aXRoIG5ldyBhbmQgaWYgZGlmZmVyZW50IHVwZGF0ZXMgaXRcclxuICAgKiBAcGFyYW0gT25qZWN0IHZhbHVlIG5ldyB2YWx1ZVxyXG4gICAqIEByZXR1cm5zIHtQcm9taXNlfVxyXG4gICAqL1xyXG4gIHNldElmTmV3KHZhbHVlKXtcclxuICAgIGNvbnN0IHNlbGYgPSB0aGlzO1xyXG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKHJlc29sdmUgPT57XHJcbiAgICAgIHNlbGYuZ2V0KCkudGhlbih2ID0+e1xyXG4gICAgICAgIGlmICh2ID09IHZhbHVlKSB7XHJcbiAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHNlbGYuc2V0KHZhbHVlKS50aGVuKHJlc29sdmUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldHMgdGhlIHZhbHVlLCB1cGRhdGVzIGl0IGFzIHVwZGF0ZUZ1bmN0aW9uIHJlcXVpcmVzIGl0XHJcbiAgICogYW5kIHNldHMgdGhlIHVwZGF0ZWQgdmFsdWVcclxuICAgKiBAcGFyYW0gRnVuY3Rpb24gdXBkYXRlRnVuY3Rpb24gZnVuY3Rpb24odmFsdWUpe3JldHVybiB1cGRhdGVkVmFsdWU7fVxyXG4gICAqIEByZXR1cm5zIHtQcm9taXNlfVxyXG4gICAqL1xyXG4gIHVwZGF0ZSh1cGRhdGVGdW5jdGlvbil7XHJcbiAgICBpZiAodHlwZW9mIHVwZGF0ZUZ1bmN0aW9uICE9PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSWxsZWdhbCBpbnZvY2F0aW9uLiBGdW5jdGlvbiBleHBlY3RlZC5cIik7XHJcbiAgICB9XHJcbiAgICBjb25zdCBzZWxmID0gdGhpcztcclxuICAgIHJldHVybiBuZXcgUHJvbWlzZShyZXNvbHZlID0+e1xyXG4gICAgICBzZWxmLmdldCgpLnRoZW4odmFsdWUgPT57XHJcbiAgICAgICAgY29uc3QgdXBkYXRlZFZhbHVlID0gdXBkYXRlRnVuY3Rpb24odmFsdWUpO1xyXG4gICAgICAgIHNlbGYuc2V0SWZOZXcodXBkYXRlZFZhbHVlKS50aGVuKHJlc29sdmUpO1xyXG4gICAgICB9KTtcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=
