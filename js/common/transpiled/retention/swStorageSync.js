"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var swStorageSync = function () {
  function swStorageSync(globalStore, storage) {
    _classCallCheck(this, swStorageSync);

    this.storage = storage;

    this.sw_retentionData = this.createStorage("sw_retentionData");
  }

  _createClass(swStorageSync, [{
    key: "createStorage",
    value: function createStorage(key) {
      return new swStorageInstance(key, this.storage);
    }
  }]);

  return swStorageSync;
}();

/**
 * Forced singleton which is an facade-like aggregation
 * of all available storage.
 * @type {SWStorage}
 */


swStorageSync = new swStorageSync(chrome.storage, chrome.storage.local);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJldGVudGlvbi9zd1N0b3JhZ2VTeW5jLmpzIl0sIm5hbWVzIjpbInN3U3RvcmFnZVN5bmMiLCJnbG9iYWxTdG9yZSIsInN0b3JhZ2UiLCJzd19yZXRlbnRpb25EYXRhIiwiY3JlYXRlU3RvcmFnZSIsImtleSIsInN3U3RvcmFnZUluc3RhbmNlIiwiY2hyb21lIiwibG9jYWwiXSwibWFwcGluZ3MiOiI7Ozs7OztJQUFNQSxhO0FBRUoseUJBQVlDLFdBQVosRUFBeUJDLE9BQXpCLEVBQWlDO0FBQUE7O0FBQy9CLFNBQUtBLE9BQUwsR0FBZUEsT0FBZjs7QUFFQSxTQUFLQyxnQkFBTCxHQUF3QixLQUFLQyxhQUFMLENBQW1CLGtCQUFuQixDQUF4QjtBQUNEOzs7O2tDQUVhQyxHLEVBQUk7QUFDaEIsYUFBTyxJQUFJQyxpQkFBSixDQUFzQkQsR0FBdEIsRUFBMkIsS0FBS0gsT0FBaEMsQ0FBUDtBQUNEOzs7Ozs7QUFHSDs7Ozs7OztBQUtBRixnQkFBZ0IsSUFBSUEsYUFBSixDQUFrQk8sT0FBT0wsT0FBekIsRUFBa0NLLE9BQU9MLE9BQVAsQ0FBZU0sS0FBakQsQ0FBaEIiLCJmaWxlIjoicmV0ZW50aW9uL3N3U3RvcmFnZVN5bmMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJjbGFzcyBzd1N0b3JhZ2VTeW5jIHtcclxuXHJcbiAgY29uc3RydWN0b3IoZ2xvYmFsU3RvcmUsIHN0b3JhZ2Upe1xyXG4gICAgdGhpcy5zdG9yYWdlID0gc3RvcmFnZTtcclxuXHJcbiAgICB0aGlzLnN3X3JldGVudGlvbkRhdGEgPSB0aGlzLmNyZWF0ZVN0b3JhZ2UoXCJzd19yZXRlbnRpb25EYXRhXCIpO1xyXG4gIH07XHJcblxyXG4gIGNyZWF0ZVN0b3JhZ2Uoa2V5KXtcclxuICAgIHJldHVybiBuZXcgc3dTdG9yYWdlSW5zdGFuY2Uoa2V5LCB0aGlzLnN0b3JhZ2UpO1xyXG4gIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEZvcmNlZCBzaW5nbGV0b24gd2hpY2ggaXMgYW4gZmFjYWRlLWxpa2UgYWdncmVnYXRpb25cclxuICogb2YgYWxsIGF2YWlsYWJsZSBzdG9yYWdlLlxyXG4gKiBAdHlwZSB7U1dTdG9yYWdlfVxyXG4gKi9cclxuc3dTdG9yYWdlU3luYyA9IG5ldyBzd1N0b3JhZ2VTeW5jKGNocm9tZS5zdG9yYWdlLCBjaHJvbWUuc3RvcmFnZS5sb2NhbCk7XHJcblxyXG4iXX0=
