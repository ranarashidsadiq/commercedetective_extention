'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var swRetention = function () {
  function swRetention(conf) {
    var _this = this;

    _classCallCheck(this, swRetention);

    this.Storage = conf.Storage;
    this.TrackGAEvents = conf.TrackGAEvents;
    this.lastRetentionDay = 28;
    this.minHoursFromInstall = 8;

    this.Storage.requestGet().then(function (data) {
      _this.data = _this.initialize(data);

      _this.report();
    });
  }

  _createClass(swRetention, [{
    key: 'initialize',
    value: function initialize(data) {
      if (data && data.installDate && data.sentDays) {
        return data;
      } else {
        data = data || {};
        data.installDate = data.installDate ? data.installDate : Date.now();
        data.sentDays = data.sentDays || {};

        this.Storage.requestSet(data);

        return data;
      }
    }
  }, {
    key: 'report',
    value: function report() {
      if (!this.data.completed) {
        var now = new Date();
        var installDate = new Date(this.data.installDate);
        var installStart = this.getDateStart(installDate);
        var todayStart = this.getDateStart(now);
        var msStartDiff = Math.abs(todayStart - installStart);
        var hoursFromTrueInstall = Math.floor((now - installDate) / (1000 * 60 * 60));
        var daysDiff = Math.floor(msStartDiff / (1000 * 60 * 60 * 24));

        if (daysDiff > 0 && daysDiff <= this.lastRetentionDay) {
          if (!this.data.sentDays[daysDiff] && hoursFromTrueInstall > this.minHoursFromInstall) {
            this.TrackGAEvents(daysDiff);
            this.data.sentDays[daysDiff] = true;
            this.Storage.requestSet(this.data);
          }

          setTimeout(this.report.bind(this), 1000 * 60 * 60);
        } else if (daysDiff > this.lastRetentionDay) {
          this.data.completed = true;
          this.Storage.requestSet(this.data);
        }
      }
    }
  }, {
    key: 'getDateStart',
    value: function getDateStart(date) {
      return new Date(date.getFullYear(), date.getMonth(), date.getHours() >= 0 && date.getHours() < 5 ? date.getDate() - 1 : date.getDate(), 5, 0, 1); //day starts at 5PM
    }
  }]);

  return swRetention;
}();

new swRetention({
  Storage: {
    requestGet: function requestGet() {
      return swStorageSync.sw_retentionData.get();
    },
    requestSet: function requestSet(data) {
      swStorageSync.sw_retentionData.set(data);
    }
  },
  TrackGAEvents: function TrackGAEvents(xDay) {
    GoogleAnalytics.trackEvent('General', 'Retained {0} day'.replace('{0}', xDay), chrome.runtime.getManifest().version);
  }
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJldGVudGlvbi9zd1JldGVudGlvbi5qcyJdLCJuYW1lcyI6WyJzd1JldGVudGlvbiIsImNvbmYiLCJTdG9yYWdlIiwiVHJhY2tHQUV2ZW50cyIsImxhc3RSZXRlbnRpb25EYXkiLCJtaW5Ib3Vyc0Zyb21JbnN0YWxsIiwicmVxdWVzdEdldCIsInRoZW4iLCJkYXRhIiwiaW5pdGlhbGl6ZSIsInJlcG9ydCIsImluc3RhbGxEYXRlIiwic2VudERheXMiLCJEYXRlIiwibm93IiwicmVxdWVzdFNldCIsImNvbXBsZXRlZCIsImluc3RhbGxTdGFydCIsImdldERhdGVTdGFydCIsInRvZGF5U3RhcnQiLCJtc1N0YXJ0RGlmZiIsIk1hdGgiLCJhYnMiLCJob3Vyc0Zyb21UcnVlSW5zdGFsbCIsImZsb29yIiwiZGF5c0RpZmYiLCJzZXRUaW1lb3V0IiwiYmluZCIsImRhdGUiLCJnZXRGdWxsWWVhciIsImdldE1vbnRoIiwiZ2V0SG91cnMiLCJnZXREYXRlIiwic3dTdG9yYWdlU3luYyIsInN3X3JldGVudGlvbkRhdGEiLCJnZXQiLCJzZXQiLCJ4RGF5IiwiR29vZ2xlQW5hbHl0aWNzIiwidHJhY2tFdmVudCIsInJlcGxhY2UiLCJjaHJvbWUiLCJydW50aW1lIiwiZ2V0TWFuaWZlc3QiLCJ2ZXJzaW9uIl0sIm1hcHBpbmdzIjoiOzs7Ozs7SUFBTUEsVztBQUNKLHVCQUFZQyxJQUFaLEVBQWtCO0FBQUE7O0FBQUE7O0FBQ2hCLFNBQUtDLE9BQUwsR0FBZUQsS0FBS0MsT0FBcEI7QUFDQSxTQUFLQyxhQUFMLEdBQXFCRixLQUFLRSxhQUExQjtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLEVBQXhCO0FBQ0EsU0FBS0MsbUJBQUwsR0FBMkIsQ0FBM0I7O0FBRUEsU0FBS0gsT0FBTCxDQUFhSSxVQUFiLEdBQTBCQyxJQUExQixDQUErQixnQkFBUTtBQUNyQyxZQUFLQyxJQUFMLEdBQVksTUFBS0MsVUFBTCxDQUFnQkQsSUFBaEIsQ0FBWjs7QUFFQSxZQUFLRSxNQUFMO0FBQ0QsS0FKRDtBQUtEOzs7OytCQUVVRixJLEVBQU07QUFDZixVQUFJQSxRQUFRQSxLQUFLRyxXQUFiLElBQTRCSCxLQUFLSSxRQUFyQyxFQUErQztBQUM3QyxlQUFPSixJQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0xBLGVBQU9BLFFBQVEsRUFBZjtBQUNBQSxhQUFLRyxXQUFMLEdBQW1CSCxLQUFLRyxXQUFMLEdBQW1CSCxLQUFLRyxXQUF4QixHQUFzQ0UsS0FBS0MsR0FBTCxFQUF6RDtBQUNBTixhQUFLSSxRQUFMLEdBQWdCSixLQUFLSSxRQUFMLElBQWlCLEVBQWpDOztBQUVBLGFBQUtWLE9BQUwsQ0FBYWEsVUFBYixDQUF3QlAsSUFBeEI7O0FBRUEsZUFBT0EsSUFBUDtBQUNEO0FBQ0Y7Ozs2QkFFUTtBQUNQLFVBQUksQ0FBQyxLQUFLQSxJQUFMLENBQVVRLFNBQWYsRUFBMEI7QUFDeEIsWUFBSUYsTUFBTSxJQUFJRCxJQUFKLEVBQVY7QUFDQSxZQUFJRixjQUFjLElBQUlFLElBQUosQ0FBUyxLQUFLTCxJQUFMLENBQVVHLFdBQW5CLENBQWxCO0FBQ0EsWUFBSU0sZUFBZSxLQUFLQyxZQUFMLENBQWtCUCxXQUFsQixDQUFuQjtBQUNBLFlBQUlRLGFBQWEsS0FBS0QsWUFBTCxDQUFrQkosR0FBbEIsQ0FBakI7QUFDQSxZQUFJTSxjQUFjQyxLQUFLQyxHQUFMLENBQVNILGFBQWFGLFlBQXRCLENBQWxCO0FBQ0EsWUFBSU0sdUJBQXVCRixLQUFLRyxLQUFMLENBQVcsQ0FBQ1YsTUFBTUgsV0FBUCxLQUF1QixPQUFPLEVBQVAsR0FBWSxFQUFuQyxDQUFYLENBQTNCO0FBQ0EsWUFBSWMsV0FBV0osS0FBS0csS0FBTCxDQUFXSixlQUFlLE9BQU8sRUFBUCxHQUFZLEVBQVosR0FBaUIsRUFBaEMsQ0FBWCxDQUFmOztBQUVBLFlBQUlLLFdBQVcsQ0FBWCxJQUFnQkEsWUFBWSxLQUFLckIsZ0JBQXJDLEVBQXVEO0FBQ3JELGNBQUksQ0FBQyxLQUFLSSxJQUFMLENBQVVJLFFBQVYsQ0FBbUJhLFFBQW5CLENBQUQsSUFBaUNGLHVCQUF1QixLQUFLbEIsbUJBQWpFLEVBQXNGO0FBQ3BGLGlCQUFLRixhQUFMLENBQW1Cc0IsUUFBbkI7QUFDQSxpQkFBS2pCLElBQUwsQ0FBVUksUUFBVixDQUFtQmEsUUFBbkIsSUFBK0IsSUFBL0I7QUFDQSxpQkFBS3ZCLE9BQUwsQ0FBYWEsVUFBYixDQUF3QixLQUFLUCxJQUE3QjtBQUNEOztBQUVEa0IscUJBQVcsS0FBS2hCLE1BQUwsQ0FBWWlCLElBQVosQ0FBaUIsSUFBakIsQ0FBWCxFQUFtQyxPQUFPLEVBQVAsR0FBWSxFQUEvQztBQUNELFNBUkQsTUFRTyxJQUFJRixXQUFXLEtBQUtyQixnQkFBcEIsRUFBc0M7QUFDM0MsZUFBS0ksSUFBTCxDQUFVUSxTQUFWLEdBQXNCLElBQXRCO0FBQ0EsZUFBS2QsT0FBTCxDQUFhYSxVQUFiLENBQXdCLEtBQUtQLElBQTdCO0FBQ0Q7QUFDRjtBQUNGOzs7aUNBRVlvQixJLEVBQU07QUFDakIsYUFBTyxJQUFJZixJQUFKLENBQ0xlLEtBQUtDLFdBQUwsRUFESyxFQUVMRCxLQUFLRSxRQUFMLEVBRkssRUFHSkYsS0FBS0csUUFBTCxNQUFtQixDQUFuQixJQUF3QkgsS0FBS0csUUFBTCxLQUFrQixDQUEzQyxHQUFnREgsS0FBS0ksT0FBTCxLQUFpQixDQUFqRSxHQUFxRUosS0FBS0ksT0FBTCxFQUhoRSxFQUlMLENBSkssRUFJRixDQUpFLEVBSUMsQ0FKRCxDQUFQLENBRGlCLENBTWQ7QUFDSjs7Ozs7O0FBR0gsSUFBSWhDLFdBQUosQ0FBZ0I7QUFDZEUsV0FBUTtBQUNOSSxnQkFBVyxzQkFBWTtBQUNyQixhQUFPMkIsY0FBY0MsZ0JBQWQsQ0FBK0JDLEdBQS9CLEVBQVA7QUFDRCxLQUhLO0FBSU5wQixnQkFBVyxvQkFBVVAsSUFBVixFQUFnQjtBQUN6QnlCLG9CQUFjQyxnQkFBZCxDQUErQkUsR0FBL0IsQ0FBbUM1QixJQUFuQztBQUNEO0FBTkssR0FETTtBQVNkTCxpQkFBYyx1QkFBVWtDLElBQVYsRUFBZ0I7QUFDNUJDLG9CQUFnQkMsVUFBaEIsQ0FBMkIsU0FBM0IsRUFBc0MsbUJBQW1CQyxPQUFuQixDQUEyQixLQUEzQixFQUFrQ0gsSUFBbEMsQ0FBdEMsRUFBK0VJLE9BQU9DLE9BQVAsQ0FBZUMsV0FBZixHQUE2QkMsT0FBNUc7QUFDRDtBQVhhLENBQWhCIiwiZmlsZSI6InJldGVudGlvbi9zd1JldGVudGlvbi5qcyIsInNvdXJjZXNDb250ZW50IjpbImNsYXNzIHN3UmV0ZW50aW9uIHtcclxuICBjb25zdHJ1Y3Rvcihjb25mKSB7XHJcbiAgICB0aGlzLlN0b3JhZ2UgPSBjb25mLlN0b3JhZ2U7XHJcbiAgICB0aGlzLlRyYWNrR0FFdmVudHMgPSBjb25mLlRyYWNrR0FFdmVudHM7XHJcbiAgICB0aGlzLmxhc3RSZXRlbnRpb25EYXkgPSAyODtcclxuICAgIHRoaXMubWluSG91cnNGcm9tSW5zdGFsbCA9IDg7XHJcblxyXG4gICAgdGhpcy5TdG9yYWdlLnJlcXVlc3RHZXQoKS50aGVuKGRhdGEgPT4ge1xyXG4gICAgICB0aGlzLmRhdGEgPSB0aGlzLmluaXRpYWxpemUoZGF0YSk7XHJcblxyXG4gICAgICB0aGlzLnJlcG9ydCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBpbml0aWFsaXplKGRhdGEpIHtcclxuICAgIGlmIChkYXRhICYmIGRhdGEuaW5zdGFsbERhdGUgJiYgZGF0YS5zZW50RGF5cykge1xyXG4gICAgICByZXR1cm4gZGF0YTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGRhdGEgPSBkYXRhIHx8IHt9O1xyXG4gICAgICBkYXRhLmluc3RhbGxEYXRlID0gZGF0YS5pbnN0YWxsRGF0ZSA/IGRhdGEuaW5zdGFsbERhdGUgOiBEYXRlLm5vdygpO1xyXG4gICAgICBkYXRhLnNlbnREYXlzID0gZGF0YS5zZW50RGF5cyB8fCB7fTtcclxuXHJcbiAgICAgIHRoaXMuU3RvcmFnZS5yZXF1ZXN0U2V0KGRhdGEpO1xyXG5cclxuICAgICAgcmV0dXJuIGRhdGE7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZXBvcnQoKSB7XHJcbiAgICBpZiAoIXRoaXMuZGF0YS5jb21wbGV0ZWQpIHtcclxuICAgICAgbGV0IG5vdyA9IG5ldyBEYXRlKCk7XHJcbiAgICAgIGxldCBpbnN0YWxsRGF0ZSA9IG5ldyBEYXRlKHRoaXMuZGF0YS5pbnN0YWxsRGF0ZSk7XHJcbiAgICAgIGxldCBpbnN0YWxsU3RhcnQgPSB0aGlzLmdldERhdGVTdGFydChpbnN0YWxsRGF0ZSk7XHJcbiAgICAgIGxldCB0b2RheVN0YXJ0ID0gdGhpcy5nZXREYXRlU3RhcnQobm93KTtcclxuICAgICAgbGV0IG1zU3RhcnREaWZmID0gTWF0aC5hYnModG9kYXlTdGFydCAtIGluc3RhbGxTdGFydCk7XHJcbiAgICAgIGxldCBob3Vyc0Zyb21UcnVlSW5zdGFsbCA9IE1hdGguZmxvb3IoKG5vdyAtIGluc3RhbGxEYXRlKSAvICgxMDAwICogNjAgKiA2MCkpO1xyXG4gICAgICBsZXQgZGF5c0RpZmYgPSBNYXRoLmZsb29yKG1zU3RhcnREaWZmIC8gKDEwMDAgKiA2MCAqIDYwICogMjQpKTtcclxuXHJcbiAgICAgIGlmIChkYXlzRGlmZiA+IDAgJiYgZGF5c0RpZmYgPD0gdGhpcy5sYXN0UmV0ZW50aW9uRGF5KSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmRhdGEuc2VudERheXNbZGF5c0RpZmZdICYmIGhvdXJzRnJvbVRydWVJbnN0YWxsID4gdGhpcy5taW5Ib3Vyc0Zyb21JbnN0YWxsKSB7XHJcbiAgICAgICAgICB0aGlzLlRyYWNrR0FFdmVudHMoZGF5c0RpZmYpO1xyXG4gICAgICAgICAgdGhpcy5kYXRhLnNlbnREYXlzW2RheXNEaWZmXSA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLlN0b3JhZ2UucmVxdWVzdFNldCh0aGlzLmRhdGEpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2V0VGltZW91dCh0aGlzLnJlcG9ydC5iaW5kKHRoaXMpLCAxMDAwICogNjAgKiA2MCk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZGF5c0RpZmYgPiB0aGlzLmxhc3RSZXRlbnRpb25EYXkpIHtcclxuICAgICAgICB0aGlzLmRhdGEuY29tcGxldGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLlN0b3JhZ2UucmVxdWVzdFNldCh0aGlzLmRhdGEpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXREYXRlU3RhcnQoZGF0ZSkge1xyXG4gICAgcmV0dXJuIG5ldyBEYXRlKFxyXG4gICAgICBkYXRlLmdldEZ1bGxZZWFyKCksXHJcbiAgICAgIGRhdGUuZ2V0TW9udGgoKSxcclxuICAgICAgKGRhdGUuZ2V0SG91cnMoKSA+PSAwICYmIGRhdGUuZ2V0SG91cnMoKSA8IDUpID8gZGF0ZS5nZXREYXRlKCkgLSAxIDogZGF0ZS5nZXREYXRlKCksXHJcbiAgICAgIDUsIDAsIDFcclxuICAgICk7IC8vZGF5IHN0YXJ0cyBhdCA1UE1cclxuICB9XHJcbn1cclxuXHJcbm5ldyBzd1JldGVudGlvbih7XHJcbiAgU3RvcmFnZTp7XHJcbiAgICByZXF1ZXN0R2V0OmZ1bmN0aW9uICgpIHtcclxuICAgICAgcmV0dXJuIHN3U3RvcmFnZVN5bmMuc3dfcmV0ZW50aW9uRGF0YS5nZXQoKTtcclxuICAgIH0sXHJcbiAgICByZXF1ZXN0U2V0OmZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgIHN3U3RvcmFnZVN5bmMuc3dfcmV0ZW50aW9uRGF0YS5zZXQoZGF0YSk7XHJcbiAgICB9XHJcbiAgfSxcclxuICBUcmFja0dBRXZlbnRzOmZ1bmN0aW9uICh4RGF5KSB7XHJcbiAgICBHb29nbGVBbmFseXRpY3MudHJhY2tFdmVudCgnR2VuZXJhbCcsICdSZXRhaW5lZCB7MH0gZGF5Jy5yZXBsYWNlKCd7MH0nLCB4RGF5KSwgY2hyb21lLnJ1bnRpbWUuZ2V0TWFuaWZlc3QoKS52ZXJzaW9uKTtcclxuICB9XHJcbn0pOyJdfQ==
