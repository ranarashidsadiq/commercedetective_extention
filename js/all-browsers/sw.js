var Countries = {
  999: {name: "World", iso2: "WO"},
  4: {name: "Afghanistan", iso2: "AF"},
  8: {name: "Albania", iso2: "AL"},
  10: {name: "Antarctica", iso2: "AQ"},
  12: {name: "Algeria", iso2: "DZ"},
  16: {name: "American Samoa", iso2: "AS"},
  20: {name: "Andorra", iso2: "AD"},
  24: {name: "Angola", iso2: "AO"},
  28: {name: "Antigua and Barbuda", iso2: "AG"},
  31: {name: "Azerbaijan", iso2: "AZ"},
  32: {name: "Argentina", iso2: "AR"},
  36: {name: "Australia", iso2: "AU"},
  40: {name: "Austria", iso2: "AT"},
  44: {name: "Bahamas", iso2: "BS"},
  48: {name: "Bahrain", iso2: "BH"},
  50: {name: "Bangladesh", iso2: "BD"},
  51: {name: "Armenia", iso2: "AM"},
  52: {name: "Barbados", iso2: "BB"},
  56: {name: "Belgium", iso2: "BE"},
  60: {name: "Bermuda", iso2: "BM"},
  64: {name: "Bhutan", iso2: "BT"},
  68: {name: "Bolivia", iso2: "BO"},
  70: {name: "Bosnia", iso2: "BA"},
  72: {name: "Botswana", iso2: "BW"},
  74: {name: "Bouvet Island", iso2: "BV"},
  76: {name: "Brazil", iso2: "BR"},
  84: {name: "Belize", iso2: "BZ"},
  86: {name: "British Indian Territory", iso2: "IO"},
  90: {name: "Solomon Islands", iso2: "SB"},
  92: {name: "Virgin Islands", iso2: "VG"},
  96: {name: "Brunei", iso2: "BN"},
  100: {name: "Bulgaria", iso2: "BG"},
  104: {name: "Myanmar", iso2: "MM"},
  108: {name: "Burundi", iso2: "BI"},
  112: {name: "Belarus", iso2: "BY"},
  116: {name: "Cambodia", iso2: "KH"},
  120: {name: "Cameroon", iso2: "CM"},
  124: {name: "Canada", iso2: "CA"},
  132: {name: "Cape Verde", iso2: "CV"},
  136: {name: "Cayman Islands", iso2: "KY"},
  140: {name: "Central African", iso2: "CF"},
  144: {name: "Sri Lanka", iso2: "LK"},
  148: {name: "Chad", iso2: "TD"},
  152: {name: "Chile", iso2: "CL"},
  156: {name: "China", iso2: "CN"},
  158: {name: "Taiwan", iso2: "TW"},
  162: {name: "Christmas Island", iso2: "CX"},
  166: {name: "Cocos Islands", iso2: "CC"},
  170: {name: "Colombia", iso2: "CO"},
  174: {name: "Comoros", iso2: "KM"},
  175: {name: "Mayotte", iso2: "YT"},
  178: {name: "Congo", iso2: "CG"},
  180: {name: "Congo Republic", iso2: "CD"},
  184: {name: "Cook Islands", iso2: "CK"},
  188: {name: "Costa Rica", iso2: "CR"},
  191: {name: "Croatia", iso2: "HR"},
  192: {name: "Cuba", iso2: "CU"},
  196: {name: "Cyprus", iso2: "CY"},
  203: {name: "Czech Republic", iso2: "CZ"},
  204: {name: "Benin", iso2: "BJ"},
  208: {name: "Denmark", iso2: "DK"},
  212: {name: "Dominica", iso2: "DM"},
  214: {name: "Dominican Republic", iso2: "DO"},
  218: {name: "Ecuador", iso2: "EC"},
  222: {name: "El Salvador", iso2: "SV"},
  226: {name: "Equatorial Guinea", iso2: "GQ"},
  231: {name: "Ethiopia", iso2: "ET"},
  232: {name: "Eritrea", iso2: "ER"},
  233: {name: "Estonia", iso2: "EE"},
  234: {name: "Faroe Islands", iso2: "FO"},
  238: {name: "Falkland Islands (Malvinas)", iso2: "FK"},
  239: {name: "South Georgia Islands", iso2: "GS"},
  242: {name: "Fiji", iso2: "FJ"},
  246: {name: "Finland", iso2: "FI"},
  248: {name: "Aland Islands", iso2: "AX"},
  250: {name: "France", iso2: "FR"},
  254: {name: "French Guiana", iso2: "GF"},
  258: {name: "French Polynesia", iso2: "PF"},
  260: {name: "French Territories", iso2: "TF"},
  262: {name: "Djibouti", iso2: "DJ"},
  266: {name: "Gabon", iso2: "GA"},
  268: {name: "Georgia", iso2: "GE"},
  270: {name: "Gambia", iso2: "GM"},
  275: {name: "Palestinian", iso2: "PS"},
  276: {name: "Germany", iso2: "DE"},
  288: {name: "Ghana", iso2: "GH"},
  292: {name: "Gibraltar", iso2: "GI"},
  296: {name: "Kiribati", iso2: "KI"},
  300: {name: "Greece", iso2: "GR"},
  304: {name: "Greenland", iso2: "GL"},
  308: {name: "Grenada", iso2: "GD"},
  312: {name: "Guadeloupe", iso2: "GP"},
  316: {name: "Guam", iso2: "GU"},
  320: {name: "Guatemala", iso2: "GT"},
  324: {name: "Guinea", iso2: "GN"},
  328: {name: "Guyana", iso2: "GY"},
  332: {name: "Haiti", iso2: "HT"},
  334: {name: "Heard Islands", iso2: "HM"},
  336: {name: "Vatican City", iso2: "VA"},
  340: {name: "Honduras", iso2: "HN"},
  344: {name: "Hong Kong", iso2: "HK"},
  348: {name: "Hungary", iso2: "HU"},
  352: {name: "Iceland", iso2: "IS"},
  356: {name: "India", iso2: "IN"},
  360: {name: "Indonesia", iso2: "ID"},
  364: {name: "Iran", iso2: "IR"},
  368: {name: "Iraq", iso2: "IQ"},
  372: {name: "Ireland", iso2: "IE"},
  376: {name: "Israel", iso2: "IL"},
  380: {name: "Italy", iso2: "IT"},
  384: {name: "Cote d Ivoire", iso2: "CI"},
  388: {name: "Jamaica", iso2: "JM"},
  392: {name: "Japan", iso2: "JP"},
  398: {name: "Kazakhstan", iso2: "KZ"},
  400: {name: "Jordan", iso2: "JO"},
  404: {name: "Kenya", iso2: "KE"},
  408: {name: "Korea", iso2: "KP"},
  410: {name: "Korea Republic", iso2: "KR"},
  414: {name: "Kuwait", iso2: "KW"},
  417: {name: "Kyrgyzstan", iso2: "KG"},
  418: {name: "Lao", iso2: "LA"},
  422: {name: "Lebanon", iso2: "LB"},
  426: {name: "Lesotho", iso2: "LS"},
  428: {name: "Latvia", iso2: "LV"},
  430: {name: "Liberia", iso2: "LR"},
  434: {name: "Libyan", iso2: "LY"},
  438: {name: "Liechtenstein", iso2: "LI"},
  440: {name: "Lithuania", iso2: "LT"},
  442: {name: "Luxembourg", iso2: "LU"},
  446: {name: "Macao", iso2: "MO"},
  450: {name: "Madagascar", iso2: "MG"},
  454: {name: "Malawi", iso2: "MW"},
  458: {name: "Malaysia", iso2: "MY"},
  462: {name: "Maldives", iso2: "MV"},
  466: {name: "Mali", iso2: "ML"},
  470: {name: "Malta", iso2: "MT"},
  474: {name: "Martinique", iso2: "MQ"},
  478: {name: "Mauritania", iso2: "MR"},
  480: {name: "Mauritius", iso2: "MU"},
  484: {name: "Mexico", iso2: "MX"},
  492: {name: "Monaco", iso2: "MC"},
  496: {name: "Mongolia", iso2: "MN"},
  498: {name: "Moldova", iso2: "MD"},
  499: {name: "Montenegro", iso2: "ME"},
  500: {name: "Montserrat", iso2: "MS"},
  504: {name: "Morocco", iso2: "MA"},
  508: {name: "Mozambique", iso2: "MZ"},
  512: {name: "Oman", iso2: "OM"},
  516: {name: "Namibia", iso2: "NA"},
  520: {name: "Nauru", iso2: "NR"},
  524: {name: "Nepal", iso2: "NP"},
  528: {name: "Netherlands", iso2: "NL"},
  530: {name: "Netherlands Antilles", iso2: "AN"},
  533: {name: "Aruba", iso2: "AW"},
  540: {name: "New Caledonia", iso2: "NC"},
  548: {name: "Vanuatu", iso2: "VU"},
  554: {name: "New Zealand", iso2: "NZ"},
  558: {name: "Nicaragua", iso2: "NI"},
  562: {name: "Niger", iso2: "NE"},
  566: {name: "Nigeria", iso2: "NG"},
  570: {name: "Niue", iso2: "NU"},
  574: {name: "Norfolk Island", iso2: "NF"},
  578: {name: "Norway", iso2: "NO"},
  580: {name: "Northern Mariana Islands", iso2: "MP"},
  581: {name: "United States Outlying Islands", iso2: "UM"},
  583: {name: "Micronesia", iso2: "FM"},
  584: {name: "Marshall Islands", iso2: "MH"},
  585: {name: "Palau", iso2: "PW"},
  586: {name: "Pakistan", iso2: "PK"},
  591: {name: "Panama", iso2: "PA"},
  598: {name: "Papua New Guinea", iso2: "PG"},
  600: {name: "Paraguay", iso2: "PY"},
  604: {name: "Peru", iso2: "PE"},
  608: {name: "Philippines", iso2: "PH"},
  612: {name: "Pitcairn", iso2: "PN"},
  616: {name: "Poland", iso2: "PL"},
  620: {name: "Portugal", iso2: "PT"},
  624: {name: "Guinea-Bissau", iso2: "GW"},
  626: {name: "Timor-Leste", iso2: "TL"},
  630: {name: "Puerto Rico", iso2: "PR"},
  634: {name: "Qatar", iso2: "QA"},
  638: {name: "Reunion", iso2: "RE"},
  642: {name: "Romania", iso2: "RO"},
  643: {name: "Russian Federation", iso2: "RU"},
  646: {name: "Rwanda", iso2: "RW"},
  652: {name: "Saint Barthelemy", iso2: "BL"},
  654: {name: "Saint Helena ", iso2: "SH"},
  659: {name: "Saint Kitts and Nevis", iso2: "KN"},
  660: {name: "Anguilla", iso2: "AI"},
  662: {name: "Saint Lucia", iso2: "LC"},
  663: {name: "Saint Martin (French part)", iso2: "MF"},
  666: {name: "Saint Pierre and Miquelon", iso2: "PM"},
  670: {name: "Saint Vincent", iso2: "VC"},
  674: {name: "San Marino", iso2: "SM"},
  678: {name: "Sao Tome and Principe", iso2: "ST"},
  682: {name: "Saudi Arabia", iso2: "SA"},
  686: {name: "Senegal", iso2: "SN"},
  688: {name: "Serbia", iso2: "RS"},
  690: {name: "Seychelles", iso2: "SC"},
  694: {name: "Sierra Leone", iso2: "SL"},
  702: {name: "Singapore", iso2: "SG"},
  703: {name: "Slovakia", iso2: "SK"},
  704: {name: "Viet Nam", iso2: "VN"},
  705: {name: "Slovenia", iso2: "SI"},
  706: {name: "Somalia", iso2: "SO"},
  710: {name: "South Africa", iso2: "ZA"},
  716: {name: "Zimbabwe", iso2: "ZW"},
  724: {name: "Spain", iso2: "ES"},
  732: {name: "Western Sahara", iso2: "EH"},
  736: {name: "Sudan", iso2: "SD"},
  740: {name: "Suriname", iso2: "SR"},
  744: {name: "Svalbard and Jan Mayen", iso2: "SJ"},
  748: {name: "Swaziland", iso2: "SZ"},
  752: {name: "Sweden", iso2: "SE"},
  756: {name: "Switzerland", iso2: "CH"},
  760: {name: "Syrian Arab Republic", iso2: "SY"},
  762: {name: "Tajikistan", iso2: "TJ"},
  764: {name: "Thailand", iso2: "TH"},
  768: {name: "Togo", iso2: "TG"},
  772: {name: "Tokelau", iso2: "TK"},
  776: {name: "Tonga", iso2: "TO"},
  780: {name: "Trinidad and Tobago", iso2: "TT"},
  784: {name: "United Arab Emirates", iso2: "AE"},
  788: {name: "Tunisia", iso2: "TN"},
  792: {name: "Turkey", iso2: "TR"},
  795: {name: "Turkmenistan", iso2: "TM"},
  796: {name: "Turks and Caicos Islands", iso2: "TC"},
  798: {name: "Tuvalu", iso2: "TV"},
  800: {name: "Uganda", iso2: "UG"},
  804: {name: "Ukraine", iso2: "UA"},
  807: {name: "Macedonia", iso2: "MK"},
  818: {name: "Egypt", iso2: "EG"},
  826: {name: "United Kingdom", iso2: "GB"},
  831: {name: "Guernsey", iso2: "GG"},
  832: {name: "Jersey", iso2: "JE"},
  833: {name: "Isle of Man", iso2: "IM"},
  834: {name: "Tanzania", iso2: "TZ"},
  840: {name: "United States", iso2: "US"},
  850: {name: "Virgin Islands, U.S.", iso2: "VI"},
  854: {name: "Burkina Faso", iso2: "BF"},
  858: {name: "Uruguay", iso2: "UY"},
  860: {name: "Uzbekistan", iso2: "UZ"},
  862: {name: "Venezuela", iso2: "VE"},
  876: {name: "Wallis and Futuna", iso2: "WF"},
  882: {name: "Samoa", iso2: "WS"},
  887: {name: "Yemen", iso2: "YE"},
  891: {name: "SERBIA Republic", iso2: "CS"},
  894: {name: "Zambia", iso2: "ZM"},
  900: {name: "European Union", iso2: "EU"},
  901: {name: "Non-spec Asia Location", iso2: "AP"},
  902: {name: "Reserved", iso2: "ZZ"}
};
var swpopup = {
  api_baseurl: "https://api.similarweb.com/",
  smSiteUrl: "",
  current_site: '',
  SEBaseUrl: localStorage.SEBaseUrl,
  openInbg: localStorage.openInBg !== "1",
  SES: {
    "google": "https://www.google.com/search?q=",
    "bing": "https://www.bing.com/search?q=",
    "yahoo": "https://search.yahoo.com/search;_ylt=Aql0BWm86x8TaumZxJCH0v.bvZx4?p=",
    "baidu": "https://www.baidu.com/s?ie=utf-8&f=8&rsv_bp=0&rsv_idx=1&tn=baidu&wd=",
    "sogou": "https://www.sogou.com/web?query=",
    "youdao": "https://www.youdao.com/search?q=",
    "wolframalpha": "https://www.wolframalpha.com/input/?i=",
    "duckduckgo": "https://duckduckgo.com/?q=",
    "yandex": "https://www.yandex.com/search/?lr=87&text="
  },
  favIconService: "https://www.google.com/s2/favicons?domain=",
  queryStr : "",
  format_number: function(num) {
    if(isNaN(num)) {
      return 0;
    }
    num = num.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while(pattern.test(num))
      num = num.replace(pattern, "$1,$2");
    return num;
  },
  format_time: function(sec) {
    sec = parseInt(sec);
    var h = Math.floor(sec / 3600);
    var m = Math.floor((sec - (h * 3600)) / 60);
    var s = sec - (h * 3600) - (m * 60);
    if(h < 10) {
      h = '0' + h;
    }
    if(m < 10) {
      m = '0' + m;
    }
    if(s < 10) {
      s = '0' + s;
    }
    return h + ':' + m + ':' + s;
  },
  format_date: function(date) {
    return Highcharts.dateFormat('%b %y', date);
  },
  format_human: function(num) {
    if(num < 1000) return num;
    var i = -1;
    var units = ['K', 'M', 'B', 'T', 'P', 'E'];
    do {
      num = num / 1000;
      i++;
    } while(num > 1000);
    return num.toFixed(1) + units[i];
  },
  get_current_site: function() {
    return swpopup.current_site;
  },
  get_domain_from_url: function(url) {
    try {
      if(url == null)
        return;
      var host = url.split('/');
      if(host.length < 3)
        return '';
      var domain = host[2];
      if(domain.indexOf("www.") == 0)
        domain = domain.substr(4);
      return domain;

    } catch(e) {
      return '';
    }
  },
   www_exist_in_url: function(url) {
    try {
      if(url == null)
        return;
      var host = url.split('/');
      if(host.length < 3)
        return '';
      var domain = host[2];
      if(domain.indexOf("www.") == 0){
         domain = true;
       }else{
         domain = false;
       }
       
      return domain;

    } catch(e) {
      return '';
    }
  },
  add_overview_data_to_ui: function(domain, data) {
    if(data.GlobalRank > 0) {
      $('#GlobalRankValue').text(swpopup.format_number(data.GlobalRank));
    } else {
      $('#GlobalRankValue').text("N/A");
      $('#GlobalRankIcon').hide();
    }

    if(data.CountryRank > 0) {
      $('#CountryRankValue').text(swpopup.format_number(data.CountryRank));
    } else {
      $('#CountryRankValue').text("N/A");
    }

    if(data.CategoryRank > 0) {
      $('#CategoryRankValue').text(swpopup.format_number(data.CategoryRank));
    } else {
      $('#CategoryRankValue').text("N/A");
    }

    $("#quickSearch").focus().attr("placeholder", swpopup.domain);
    if(data.Category) {
      var subTitle = data.Category;
      if(subTitle == null || subTitle == "") {
        subTitle = "Unknown Category";
      }
      subTitle = subTitle.replace(/_/g, " ");
      subTitle = subTitle.replace(/\//g, " > ");

      var cat_link = data.Category;
      cat_link = cat_link.replace("/", "~");
      cat_link = cat_link.replace("/", "~");

      $('#CategorySubTitle').text(subTitle).attr("category", cat_link).attr("title", subTitle).addClass('link').click(function() {
        _browser.openTab(SW_URLS.LITE.topWebsites($(this).attr('category')));
      });
      $("#CategoryIcon").addClass(data.Category.split('/')[0].toLowerCase()).css('display', 'inline-block');
    }

    try {
      var countryName = '';
      var countryClass = '';
      try {
        countryName = Countries[data.Country].name;
        countryClass = Countries[data.Country].iso2.toLowerCase();
      } catch(e) {
      }

      $("#CountrySubTitle").attr("cname", Countries[data.Country].name).addClass('link');
      $("#CountrySubTitle").text('In ' + countryName).click(function() {
        _browser.openTab(SW_URLS.LITE.topWebsites(null, $("#CountrySubTitle").attr("cname")));
      });

      if(countryClass) {
        $("#CountryFlag").addClass(countryClass).css('display', 'inline-block');
      }
    } catch(e) {
    }

    // fix string widths to max
    $('#rank .row').each(function() {
      $(this).find('.name').width($('#rank').width() - $(this).find('.value').width() - $(this).find('.etc').width() - 6);
    });

    // fix #CategorySubTitle length
    var title = $('#CategorySubTitle').text();
    var col_width = $('#CategorySubTitle').closest('.col').width();
    var i = 1;
    while(col_width < $('#CategorySubTitle').width()) {
      var text = title.substring(0, title.length - i) + '...';
      $('#CategorySubTitle').text(text);
      i++;
      if(i > title.length) break;
    }

    if(data.IsSiteVerified) {
      $(".verifiedData").show();
    }
  },
  add_traffic_share_data_to_ui: function(domain, data) {
    if(!data.TrafficShares || (!data.TrafficShares.Direct && !data.TrafficShares.Search && !data.TrafficShares.Social 
      && !data.TrafficShares.Mail && !data.TrafficShares["Paid Referrals"] && !data.TrafficShares.Referrals)) {
      $('#tab-traffic-content').find('.error_page').css('display', 'block');
      invalidate_tab('tab-traffic');
      return;
    }
    swpopup.generate_chart(data.TrafficShares);
  },
  add_visits_data_to_ui: function(domain, data) {
    var widgetdata = [];
    for(var i = 0; i < data.Values.length; i++) {
      widgetdata[i] = [
        Date.parse(data.Values[i].Date), data.Values[i].Value];
    }

    Highcharts.setOptions({
      lang: {
        numericSymbols: ['E']
      }
    });

    $('#graph-traffic').highcharts({
      title: null,
      chart: {
        type: "line",
        reflow: !0,
        animation: false,
        marginBottom: 39
      },
      tooltip: {
        enabled: !0,
        borderWidth: 1,
        backgroundColor: "#fff",
        style: {
          color: "#494949",
          fontSize: "11px",
          fontFamily: "Arial",
          padding: "5px"
        }
      },
      xAxis: {
        x: 0,
        y: 0,
        lineColor: "#CCCCCC",
        gridLineColor: "#eaeaea",
        type: "datetime",
        title: {
          text: null
        },
        tickmarkPlacement: "on",
        showFirstLabel: true,
        labels: {
          y: 30,
          style: {
            fontFamily: "arial",
            color: "#cccccc"
          },
          formatter: function() { return swpopup.format_date(this.value); }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: null
        },
        labels: {
          style: {
            fontFamily: "arial",
            color: "#999999"
          },
          enabled: !0
        }
      },
      colors: ["#066799"],
      plotOptions: {
        line: {
          shadow: !1,
          dataLabels: {
            enabled: !1
          },
          events: {
            legendItemClick: function() {
              return !1;
            }
          },
          enableMouseTracking: !0,
          marker: {
            enabled: !0,
            symbol: "circle",
            radius: 4,
            lineWidth: 2
          }
        },
        series: {
          animation: true,
          animation: false, // true broke Firefox version > 56
          states: {
            hover: {
              halo: {
                attributes: {
                  fill: "#FFFFFF"
                }
              }
            }
          }
        }
      },
      series: [{
        name: domain.length > 36 ? domain.substring(0, 36) + '...' : domain,
        data: widgetdata
      }],
      legend: {
        enabled: !1
      },
      credits: {
        enabled: !1
      }
    });



     if(data.Values.length>0){
          if(data.Values[data.Values.length - 1].Value > 0) {
          $('#EstimatedVisits').text(swpopup.format_human(data.Values[data.Values.length - 1].Value));
           localStorage.EstimatedVisits = data.Values[data.Values.length - 1].Value;
        }
      }else{
        localStorage.EstimatedVisits = 4000;
      }
    
  },
  add_visitduration_data_to_ui: function(domain, data) {
    if(!data.Values.length) {
      return;
    }

    if(data.Values[0].Value > 0) {
      $('#TimeOnSite').text(swpopup.format_time(data.Values[0].Value));
    }
  },
  add_pageviews_data_to_ui: function(domain, data) {
    if(!data.Values.length) {
      return;
    }

    if(data.Values[0].Value > 0) {
      $('#PageViews').text(data.Values[0].Value.toFixed(2));
    }
  },
  add_bouncerate_data_to_ui: function(domain, data) {
    if(!data.Values.length) {
      return;
    }

    if(data.Values[0].Value > 0) {
      $('#BounceRate').text(formattedPercentage(data.Values[0].Value) + '%');
    }
  },
  add_traffic_data_to_ui: function(domain, data) {

    if(data){


    if(!data.TopCountryShares.length) {
      $('#tab-geo-content').find('.error_page').css('display', 'block');
      invalidate_tab('tab-traffic');
      return;
    }

    var code, share;
    var graph_data = [];
    for(var i = 0; i < Math.min(8, data.TopCountryShares.length); i++) {
      code = data.TopCountryShares[i].CountryCode;
      share = data.TopCountryShares[i].TrafficShare;
      var item = $('<div>', {
        class: "country-item"
      });
      $('<img>', {
        src: 'images/flags/' + Countries[code].iso2.toUpperCase() + '.png',
        class: 'flag'
      }).appendTo(item);
      $('<div>', {
        class: "country-share"
      }).text(formattedPercentage(share) + '%').appendTo(item);
      $('<div>', {
        class: "country-name"
      }).text(Countries[code].name).appendTo(item);
      item.appendTo($('#leading-countries'));
    }

    $('#lead-country-share').text(formattedPercentage(data.TopCountryShares[0].TrafficShare) + '%');
    $('#lead-country-name').text(Countries[data.TopCountryShares[0].CountryCode].name);
    $('#lead-country-domain').text(domain);


    var moreButton = $('#tab-geo-content').find('.fulldetailsBtn');
    swpopup.showHookButton(moreButton, data.uniCount);

    // all countries:
    for(var i = 0; i < data.TopCountryShares.length; i++) {
      code = data.TopCountryShares[i].CountryCode;
      share = data.TopCountryShares[i].TrafficShare;
      if(share > 0.0001) {
        graph_data[i] = {
          code: Countries[code].iso2.toUpperCase(),
          value: formattedPercentage(share)
        };
      }
    }

    $('#graph-regions').highcharts('Map', {
      chart: {
        spacing: [0, 0, 0, 0],
        margin: [0, 0, 0, 0]
      },
      title: null,
      mapNavigation: {
        enabled: false
      },
      navigation: {
        buttonOptions: {
          enabled: false
        }
      },
      colorAxis: {
        min: 1,
        max: formattedPercentage(data.TopCountryShares[0].TrafficShare),
        type: 'logarithmic',
        maxColor: '#3999c1'
      },
      plotOptions: {
        map: {
          nullColor: '#f5f5f5'
        }
      },
      series: [{
        data: graph_data,
        mapData: Highcharts.maps['custom/world'],
        joinBy: ['iso-a2', 'code'],
        name: '<b>' + ucfirst(domain.length > 36 ? domain.substring(0, 36) + '...' : domain) + '</b> traffic in last 3 months',
        states: {
          hover: {
            color: '#3999c1',
            borderColor: '#7e9ba7',
            borderWidth: 2
          }
        },
        tooltip: {
          valueSuffix: '%'
        }
      }],
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      }
    });

}
  },
  add_sales_data_to_ui: function(domain, data , productsCount) {

  $.get('https://app.commercedetective.com/home/checkIfMember?ExtentionuserAgent='+localStorage.ExtentionuserAgent, function(response) {
    var siteLen = data.products.length;
    var image = data.products[0].images[0].src;

    console.log(response);
 
      if(response){
         $('#tab-referral-content').find('.error_page').css('display', 'block');
          for(var i = 0; i < siteLen; i++) {
              var src='';
              if(data.products[i].images[0].src){
                src =data.products[i].images[0].src;
              }else{
                src = "images/default-site.jpg";
              }
             var item = $('<li class="sale_li">');
             var monthlySale =parseInt(parseInt(localStorage.EstimatedVisits/((i+1)*10000)*100) *data.products[i].variants[0].price);
           
            $( "<div class='main_des'><a target='_blank' href='"+ domain+'/products/'+data.products[i].handle+"' ><img src='"+src+"' style='width: 65px;height: 65px;border-radius: 50%;'/><p class='sal_p'>"+data.products[i].title.substring(0, 30)+'<br>'+data.products[i].title.substring(30, 60)+"<br>"+"<span class='price_sale'>Price : $"+data.products[i].variants[0].price+"</span></p></a><ul class='table_sales_in'> <li>$"+swpopup.format_number(monthlySale)+"+" +"</li><li>"+parseInt(localStorage.EstimatedVisits/((i+1)*10000)*100)+"+</li></ul></div>" ).appendTo(item);
            item.appendTo($('#tab-traffic-ids'));
          }

      }else{
         

         $('#bestpaid').text(" Bestselling Products Last Week For Paid Members Only");

             var item = $('<img src="images/home.png"/>');
            item.appendTo($('#tab-traffic-ids'));

         
      }
   
   

      });
    
    $.get('https://app.commercedetective.com/home/addShopInfo?domain='+domain+'&traffic='+localStorage.EstimatedVisits+'&productCount='+productsCount+'&image='+image, function(xml) {
 
    });




    
  },
  add_referal_data_to_ui: function(domain, data) {
      $('#tab-referral-content').find('.error_page').css('display', 'block');
      $.get('https://app.commercedetective.com/home/ref_extentions?domain='+domain+'&traffic='+localStorage.EstimatedVisits, function(xml) {
     

     var data = JSON.parse(xml);

     var siteLen = data.extentions.length;
     for(var i = 0; i < siteLen; i++) {
      
      var des = data.extentions[i].description;
      var item = $('<li>');
      $( "<a href='"+data.extentions[i].link+"' target='_blank'><p class='ref_head'>"+data.extentions[i].name+"</p></a>" ).appendTo(item);
      $( "<div class='main_des'><img src='"+data.extentions[i].image+"' style='width: auto;height: 65px;border-radius: 50%;'><p class='des_p'>"+des.substring(0, 80)+"<br>"+des.substring(80, 160)+"<br>"+des.substring(160, 240)+"</p></div>" ).appendTo(item);
      $( '<a target="_blank" href="https://app.commercedetective.com/shops/browse?appId='+domain+'"><p class="pro"><span class="pro_span">Pro</span> Shops Using this App '+data.extentions[i].name.substring(0, 50)+'</p> </a>' ).appendTo(item);
      item.appendTo($('#referrals-id'));
    }

    });

  },
add_App_referal_data_to_ui: function(domain, data) {

$('#tab-referral-content').find('.error_page').css('display', 'block');
      $.get('https://app.commercedetective.com/home/ref_app', function(xml) {
     
      var data = JSON.parse(xml);

     var siteLen = data.extentions.length;
     for(var i = 0; i < siteLen; i++) {
      var des = data.extentions[i].description;

      var item = $('<li>');
      $( "<a href='"+data.extentions[i].link+"' target='_blank'><p class='ref_head'>"+data.extentions[i].name+"</p></a>" ).appendTo(item);
      $( "<div class='main_des'><img src='"+data.extentions[i].image+"' style='width: auto;height: 65px;border-radius: 50%;'><p class='des_p'>"+des.substring(0, 80)+"<br>"+des.substring(80, 160)+"<br>"+des.substring(160, 240)+"</p></div>" ).appendTo(item);
      $( '<a target="_blank" href="https://app.commercedetective.com/shops/browse?appId='+domain+'"><p class="pro"><span class="pro_span">Pro</span> Shops Using this App '+data.extentions[i].name.substring(0, 50)+'</p> </a>' ).appendTo(item);

      item.appendTo($('#referrals-id-app'));
    }
    });

  },
  add_orgsearch_data_to_ui: function(domain, data) {
    if(!data.Data.length) {
      $('#top-organic-kw').find('.no-data').show();
      if($('#top-paid-kw').find('.no-data').attr("style").indexOf("display: block") !== -1) {
        $('#tab-search-content').find('.error_page').show();
        invalidate_tab('tab-search');
      }
      return;
    }
    var kw, percent;
    for(var i = 0; i < data.Data.length; i++) {
      kw = data.Data[i].SearchTerm;
      percent = data.Data[i].Visits;
      var item = $('<li>');

      $('<div>', {
        class: "text link kw",
        title: ucfirst(kw),
        ref: (localStorage.SEBaseUrl || "https://www.google.com/search?q=") + encodeURIComponent(kw)
      }).text(ucfirst(kw.length <= 35 ? kw : kw.substring(0, 32) + '...')).appendTo(item);
      var bar = $('<div>', {
        class: "progress-bar",
        title: formattedPercentage(percent) + "%"
      });
      $('<div>', {
        class: "progress-value",
        style: "width:" + formattedPercentage(percent) + "%"
      }).appendTo(bar);
      bar.appendTo(item);
      item.appendTo($('#top-organic-kw-list'));
    }
    var moreButton = $('#top-organic-kw').find('.fulldetailsBtn');
    swpopup.showHookButton(moreButton, data.uniCount);
  },
  add_paidsearch_data_to_ui: function(domain, data) {
    if(!data.Data.length) {
      $('#top-paid-kw').find('.no-data').show();
      if($('#top-organic-kw').find('.no-data').attr("style").indexOf("display: block") !== -1) {
        $('#tab-search-content').find('.error_page').show();
        invalidate_tab('tab-search');
      }
      return;
    }
    var kw, percent;
    for(var i = 0; i < data.Data.length; i++) {
      kw = data.Data[i].SearchTerm;
      percent = data.Data[i].Visits;
      var item = $('<li>');
      $('<div>', {
        class: "text link kw",
        title: ucfirst(kw),
        ref: (localStorage.SEBaseUrl || "https://www.google.com/search?q=") + encodeURIComponent(kw)
      }).text(ucfirst(kw.length <= 35 ? kw : kw.substring(0, 32) + '...')).appendTo(item);
      var bar = $('<div>', {
        class: "progress-bar",
        title: formattedPercentage(percent) + "%"
      });
      $('<div>', {
        class: "progress-value",
        style: "width:" + formattedPercentage(percent) + "%"
      }).appendTo(bar);
      bar.appendTo(item);
      item.appendTo($('#top-paid-kw-list'));
    }
    var moreButton = $('#top-paid-kw').find('.fulldetailsBtn');
    swpopup.showHookButton(moreButton, data.uniCount);
  },
  add_social_data_to_ui: function(domain, data) {
    if(!data.SocialSources.length) {
      $('#tab-social-content').find('.error_page').css('display', 'block');
      invalidate_tab('tab-social');
      return;
    }
    var src, percent;
    for(var i = 0; i < Math.min(data.SocialSources.length, 5); i++) {
      src = data.SocialSources[i].Source;
      percent = data.SocialSources[i].Value;
      var item = $('<li>');
      $('<div>', {
        class: "text",
        title: ucfirst(src)
      }).text(ucfirst(src)).prepend('<img class="socialIcon" src="' + data.SocialSources[i].Icon + '">').appendTo(item);
      var bar = $('<div>', {
        class: "progress-bar",
        title: formattedPercentage(percent) + "%"
      });
      $('<div>', {
        class: "progress-value",
        style: "width:" + formattedPercentage(percent) + "%"
      }).appendTo(bar);
      bar.appendTo(item);
      $('<div>', {
        class: "value"
      }).text(formattedPercentage(percent) + "%").appendTo(item);
      item.appendTo($('#social-sources-list'));
    }
  },
  add_similarsites_data_to_ui: function(domain, data) {
   
  
    var siteLen = data.products.length;
    if(!siteLen) {
      $('#tab-similar-content').find('.error_page').css('display', 'block');
      invalidate_tab('tab-similar');
      return;
    }
    siteLen = siteLen > 28 ? 28 : siteLen;

    for(var i = 0; i < siteLen; i++) {
       var src ='';
       if(data.products[i].images[0]){
          src =data.products[i].images[0].src;
        }else{
          src = "images/default-site.jpg";
        }
      var site = data.products[i].title;
      var item = $('<li>');
      $('<img>', {
        src: src,
        width: 136,
        height: 98,
        class: "similar-site",
        id: 'thumb-for-' + site.replace('.', '')
      }).appendTo(item);
      var icon = $('<div>', {
        class: "icon",
        ref: site
      }).appendTo(item);
      var text = $('<div>', {
        class: "text"
      }).append($("<a>", {
        title: site,
        ref: 'http://' + domain+'/products/'+data.products[i].handle
      }).text(site.length <= 17 ? site : site.substring(0, 15) + '...')).appendTo(item);

      var text = $('<div>', {
      class: "price"
      }).append($("<br><a>", {
        title: site,
        ref: 'http://' + domain+'/products/'+data.products[i].handle
      }).text('Price : '+ data.products[i].variants[0].price)).appendTo(item);




      $('<div>', {
        class: "link-icon",
        title: site,
        ref: 'http://' + domain+'/products'
      }).click(function() {
        _browser.openTab($(this).attr("ref"));
      }).appendTo(text);
      item.appendTo($('#similar-sites-list'));
    }

    $("#tab-similar-content").find(".text a").click(function(e) {
      swpopup.openExternalLink($(this), e);
    });
    $.get(swpopup.smSiteUrl + domain, function(xml) {
      if($(xml).find("status").text() != 0) {
        return;
      }
      $(xml).find("relatedsites").find("site").each(function() {
        var sitedata = {
          url: 'http://' + domain+'/products/'+data.products[i].handle,
          icon: $(this).find("icon").text(),
          image: $(this).find("image").text()
        };
        $('.icon[ref="' + sitedata.url + '"]').find("img").attr('src', sitedata.icon);
      });
    });
  },
  add_ads_network_data_to_ui: function(domain, data) {
    if(data){
    if(!data.Data.length) {
      $('#top-ads-network').find('.no-data').css('display', 'block');
      if($('#top-ads-publisher').find('.no-data').attr("style").indexOf("display: block") !== -1) {
        $('#tab-ads-content').find('.error_page').css('display', 'block');
        invalidate_tab('tab-ads');
      }
      return;
    }

    var kw, percent;
    for(var i = 0; i < data.Data.length; i++) {
      kw = data.Data[i].Site;
      percent = data.Data[i].Display;
      var item = $('<li>');

      $('<div>', {
        class: "text link kw",
        title: ucfirst(kw),
        ref: (localStorage.SEBaseUrl || "https://www.google.com/search?q=") + encodeURIComponent(kw)
      }).text(ucfirst(kw.length <= 35 ? kw : kw.substring(0, 32) + '...')).appendTo(item);
      var bar = $('<div>', {
        class: "progress-bar",
        title: formattedPercentage(percent) + "%"
      });
      $('<div>', {
        class: "progress-value",
        style: "width:" + formattedPercentage(percent) + "%"
      }).appendTo(bar);
      bar.appendTo(item);
      item.appendTo($('#top-ads-network-list'));
    }
        }
  },
  add_ads_publisher_data_to_ui: function(domain, data) {
    if(data){

    if(!data.Data.length) {
      $('#top-ads-publisher').find('.no-data').css('display', 'block');
      if($('#top-ads-network').find('.no-data').attr("style").indexOf("display: block") !== -1) {
        $('#tab-ads-content').find('.error_page').css('display', 'block');
        invalidate_tab('tab-ads');
      }
      return;
    }

    var pb, percent;
    for(var i = 0; i < data.Data.length; i++) {
      pb = data.Data[i].Site;
      percent = data.Data[i].Display;
      var item = $('<li>');

      var text = $('<div>', {
        class: "text link",
        title: ucfirst(pb),
        domain: pb,
        ref: SW_URLS.LITE.websiteAnalysisOverview(pb)
      }).text(ucfirst(pb.length <= 35 ? pb : pb.substring(0, 32) + '...')).appendTo(item).click(function(e) {
        swpopup.openExternalLink($(this), e);
      });
      $('<div>', {
        class: "link-icon",
        ref: 'http://' + pb
      }).click(function() {
        _browser.openTab($(this).attr('ref'));
        return false;
      }).appendTo(text);
      var bar = $('<div>', {
        class: "progress-bar",
        title: formattedPercentage(percent) + "%"
      });
      $('<div>', {
        class: "progress-value",
        style: "width:" + formattedPercentage(percent) + "%"
      }).appendTo(bar);
      bar.appendTo(item);
      item.appendTo($('#top-ads-publisher-list'));
    }
    var moreButton = $('#top-ads-publisher').find('.fulldetailsBtn');
    swpopup.showHookButton(moreButton, data.uniCount);

  }
  },
  _on_data_loaded: function(element_ids) {
    for(var i = 0; i < element_ids.length; i++) {
      var element_id = element_ids[i];
      tabs_loading[element_id]--;
      if(!tabs_loading[element_id]) {
        $('#' + element_id).find('.loading').css('display', 'none');
      }
    }
  },
  generate_chart: function(data) {
    var sources = ["Direct", "Search", "Social", "Mail", "Paid Referrals", "Referrals"];

    sources.forEach(function (sourceName) {
      var sourceNumber = (data[sourceName] * 100).toFixed(2);
      var sourceString = sourceName.toLowerCase().replace(" ", "-");

      addColumn(0, sourceNumber, sourceString);
    });
  },

  showHookButton: function(btn, count) {
    var leftCount = count - 5;
    if(leftCount > 0) {
      btn.find("span").text(swpopup.format_number(leftCount));
      btn.show();
    }
  },

  openExternalLink: function(sel, e) {
    var url = sel.attr("ref"),
      domain = sel.attr("domain");
    if(localStorage.showInfo === "1") {
      showInfo("link", domain, e);
    } else {
      _browser.openTab(localStorage.isPro !== "1" ? url : SW_URLS.PRO.websiteAnalysisOverview(domain));
    }
  }
};

var chart = $("body").find(".chart");

function formattedPercentage (value) {
  return (value * 100).toFixed(2);
}

function ucfirst (value) {
  return value ? value[0].toUpperCase() + value.substring(1) : '';
}

function faviconize (e, dim) {
  dim = dim || 16;
  var sel = $(e), faveIconUrl = swpopup.favIconService + sel.attr('ref');
  sel.css('background', 'none').html($('<img>', {
    width: dim,
    height: dim,
    src: faveIconUrl
  }));
}

function addColumn (slot, data, where) {
  var k = $('.js-sources-graph').height() / 100;
  var _where = $(".js-sources-" + where).find(".js-sources-graph");
  var _series = $("<div class='series'></div>");
  var _label = $("<span class='label slot'></span>");
  var _percentage = data;
  var _seriesCount;

  _series.css("height", (parseFloat(_percentage) <= 1.0) ? 1.0 : _percentage + "%");
  _series.css("margin-top", k * (100 - _series.height()));
  _series.addClass("slot" + slot);

  _series.data("slot", slot);

  _label.html(_percentage + "%");
  _label.addClass("slot" + slot);

  _where.append(_series);
  _where.append(_label);
  _where.addClass("size1Of1");

  try {
    _label.data("slot", slot);
    _label.css("bottom", _percentage + "%");

    if(_label.hasClass("high")) {
      _label.css("bottom", "5px");
    }

    $(window).trigger("resize");
  } catch(ex) {
  }

  return this;
}

function showSESelector (kw) {
  var overlay = $(".overlay"), SESelector = $(".SESelector");
  overlay.fadeIn(function() {
    SESelector.fadeIn();
  });
  $("#set").click(function() {
    var se = $("#defaultSE").val(), baseUrl = swpopup.SES[se];
    localStorage.SEBaseUrl = baseUrl;
    swpopup.SEBaseUrl = baseUrl;
    _browser.openTab(baseUrl + encodeURIComponent(kw));
  });
}

// run once
tabs_loading = {};
tabs_loaded = {};
tabs_invalid = {};
load_tab = function(id, data) {


  tabs_loaded[id] = 1;
 // tabs_invalid[id] = 0;	// default
  switch(id) {
    case 'tab-overview':
      tabs_loaded['tab-traffic'] = 1;
      tabs_loaded['tab-similar'] = 1;
      swpopup.add_overview_data_to_ui(swpopup.domain, data.rankOverView);      
      swpopup.add_visits_data_to_ui(swpopup.domain, data.visits);      
      swpopup.add_bouncerate_data_to_ui(swpopup.domain, data.bouncerate);
      swpopup.add_visitduration_data_to_ui(swpopup.domain, data.visitduration);
      swpopup.add_pageviews_data_to_ui(swpopup.domain, data.pageviews);

      swpopup._on_data_loaded(['tab-overview-content']);
    case 'tab-traffic':
      swpopup.add_traffic_share_data_to_ui(swpopup.domain, data.rankOverView);
      swpopup._on_data_loaded(['tab-traffic-content']);
      break;
    case 'tab-geo':
      swpopup.add_traffic_data_to_ui(swpopup.domain, data.traffic);
      swpopup._on_data_loaded(['tab-overview-content', 'tab-geo-content']);
      break;
    case 'tab-referral':
      swpopup.add_referal_data_to_ui(swpopup.domain, data.leadingreferringsites);
      swpopup._on_data_loaded(['tab-referral-content']);
      break;
    case 'tab-appreferral':
      swpopup.add_App_referal_data_to_ui(swpopup.domain, data.leadingreferringsites);
      swpopup._on_data_loaded(['tab-appreferral-content']);
      break;
    case "tab-bestselling":
     // isDisbaled=((!data.orgsearch.Data || !data.orgsearch.Data.length) && (!data.paidsearch.Data || !data.paidsearch.Data.length ) ) 
      break;
    case 'tab-search':
      swpopup.add_paidsearch_data_to_ui(swpopup.domain, data.paidsearch);
      swpopup.add_orgsearch_data_to_ui(swpopup.domain, data.orgsearch);
      swpopup._on_data_loaded(['tab-search-content']);
      break;
    case 'tab-social':
      swpopup.add_social_data_to_ui(swpopup.domain, data.socialreferringsites);
      swpopup._on_data_loaded(['tab-social-content']);
      break;
    case 'tab-similar':
      // swpopup.add_similarsites_data_to_ui(swpopup.domain, data.rankOverView);
      // swpopup._on_data_loaded(['tab-similar-content']);
      break;
    case 'tab-ads':
      swpopup.add_ads_network_data_to_ui(swpopup.domain, data.adsNetWork);
      swpopup.add_ads_publisher_data_to_ui(swpopup.domain, data.adsPublisher);
      swpopup._on_data_loaded(['tab-ads-content']);
      break;
  }
};

activate_tab = function(id) {
  var tabs = $('#tabs > ul > li');
  var items = $('#tabs-content > ul > li');

  for(var i = 0; i < tabs.length; i++) {
    $(tabs[i]).removeClass("tab-active")
    items[i].setAttribute('style', 'display: none');
  }
  $('#'+ id).addClass('tab-active');
  document.getElementById(id + '-content').setAttribute('style', 'display: block');
  document.getElementById(id + '-content').setAttribute('class', 'content-active');
};

invalidate_tab = function(id) {
  tabs_invalid[id] = 1;
};

loadDataWithNewAPI = function(forceReload, renderingTabs) {
  chrome.runtime.sendMessage({
    message: "GetDomainData", domain: swpopup.domain, forceReload: forceReload
  }, renderingTabs);
};

var getParameterByName = function(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

window.onload = function() {
 

  // $("#test3").text("Not a Valid Shopify Store");
  // var rana = "rashid";
  // var browserZoom = Number(getParameterByName('zoom'));
  // if(!isNaN(browserZoom) && browserZoom !== 1 && browserZoom !== 0) {
  //   document.body.style.zoom = 1 / browserZoom;
  // }

  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    swpopup.current_site = tabs[0].url;
    swpopup.domain = swpopup.get_domain_from_url(swpopup.current_site);

    var check_if_www_in_url = swpopup.www_exist_in_url(swpopup.current_site);
    var firstUrl = 'http://';
    if(check_if_www_in_url){
        firstUrl = 'http://www.';
    }else{

    }
    

      localStorage.showInfo = 0;

       

       var urlsite = swpopup.current_site;

       var match = urlsite.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
           urlsite = match[2];
        }
        else {
            urlsite =  null;
        }
        var productsCount=0;

        
        $.getJSON(firstUrl+urlsite+"/collections.json", function( data ) {
          ///gand
         
       }).done(function(data) {
         
          var collectionsLen = data.collections.length;

          
          for(var i = 0; i < collectionsLen; i++) {
            productsCount = data.collections[i].products_count + productsCount;
          }

       })



      $.getJSON(firstUrl+urlsite+"/products.json", function( data ) {
          
         
       }).done(function(data) {
      // fetch_extention();
        //add sales data to ui
       setTimeout(function() { swpopup.add_sales_data_to_ui(firstUrl+swpopup.domain, data , productsCount , firstUrl); }, 2000);
       
       swpopup.add_similarsites_data_to_ui(swpopup.domain, data , );
       swpopup._on_data_loaded(['tab-similar-content']);
       renderingTab(false);


        })
        .fail(function() {
           $("#tab-overview-content").find('.error_page').css('display', 'block').text("Not a Valid Shopify Store").find(".reload").hide();      
        })
        .always(function(data) {
             
        });


     
  })
};
// parse api data to json to rendering tabs
function parseNewJsonData (jsonData) {
    var tabsData =[];
  if(jsonData){
     // small site
  tabsData.SmallSite = {
    IsSmall: jsonData.IsSmall,
    SmallSiteTitle:jsonData.SmallSiteMessageTitle,
    SmallSiteMessage: jsonData.SmallSiteMessage
  }

  //overview
  tabsData.rankOverView = {
    //FavIcon: xml.find,
    Category: jsonData.Category,
    CategoryRank: jsonData.CategoryRank.Rank,
    Country: jsonData.CountryRank.Country,
    CountryRank: jsonData.CountryRank.Rank,
    GlobalRank: jsonData.GlobalRank.Rank,
    //LastAvgTrafficReach:?
    SimilarSites: jsonData.SimilarSites,
    Title: jsonData.Title,
    //TrafficReach:?
    TrafficShares: jsonData.TrafficSources,
    IsSiteVerified: jsonData.IsSiteVerified
  };

  tabsData.visits = {
    Values: (function() {
      var ts = [];
      for(var k in jsonData.EstimatedMonthlyVisits) {
        ts.push({Date: k, Value: jsonData.EstimatedMonthlyVisits[k]});
      }

      return ts;
    })(),
    LatestPeriod: jsonData.Engagments && jsonData.Engagments.Year && jsonData.Engagments.Month
      ? new Date(jsonData.Engagments.Year, jsonData.Engagments.Month)
      : new Date()
  };
  tabsData.visitduration = {
    Values: [{
      Date: jsonData.Engagments.Year + "-" + jsonData.Engagments.Month + "-1",
      Value: jsonData.Engagments.TimeOnSite
    }]
  };
  tabsData.pageviews = {
    Values: [{
      Date: jsonData.Engagments.Year + "-" + jsonData.Engagments.Month + "-1",
      Value: jsonData.Engagments.PagePerVisit
    }]
  };

  tabsData.bouncerate = {
    Values: [
      {
        Value: jsonData.Engagments.BounceRate,
        Visits: jsonData.Engagments.Visits
      }
    ]
  };
  //GEO
  tabsData.traffic = {
    Category: jsonData.Category,
    CategoryRank: jsonData.CategoryRank.Rank,
    Country: jsonData.CountryRank.Country,
    CountryRank: jsonData.CountryRank.Rank,
    GlobalRank: jsonData.GlobalRank.Rank,
    Date: (new Date()).getMonth() + "/" + (new Date()).getFullYear(),
    TopCountryShares: (function() {
      var ts = [];
      $.each(jsonData.TopCountryShares, function(i, js) {
        ts.push({
          CountryCode: js.Country,
          TrafficShare: js.Value
        });
      });
      return ts;
    })(),
    //TrafficReach: ?
    uniCount: jsonData.TotalCountries,
    TrafficShares: {
      Values: (function() {
        var ts = [];
        for(var k in jsonData.EstimatedMonthlyVisits) {
          ts.push({Date: k, Value: jsonData.EstimatedMonthlyVisits[k]});
        }

        return ts;
      })()
    }
  };
  //referrer data
  tabsData.leadingdestinationsites = {
    Sites: (function() {
      var td = [];
      $.each(jsonData.TopDestinations, function(i, js) {
        td.push(js.Site);
      });
      return td;
    })(),
    destCount: jsonData.TotalDestinations
  };
  tabsData.leadingreferringsites = {
    Sites: (function() {
      var td = [];
      $.each(jsonData.TopReferring, function(i, js) {
        td.push(js.Site);
      });
      return td;
    })(),
    referCount: jsonData.TotalReferring
  };

  //search
  tabsData.orgsearch = {
    Data: (function() {
      var oss = [];
      $.each(jsonData.TopOrganicKeywords, function(i, os) {
        oss.push({
          SearchTerm: os.Keyword,
          Visits: os.Value,
          Change: null
        });
      });
      return oss;
    })(),
    uniCount: jsonData.OrganicKeywordsRollingUniqueCount
  };
  tabsData.paidsearch = {
    Data: (function() {
      var oss = [];
      $.each(jsonData.TopPaidKeywords, function(i, os) {
        oss.push({
          SearchTerm: os.Keyword,
          Visits: os.Value,
          Change: null
        });
      });
      return oss;
    })(),
    uniCount: jsonData.PaidKeywordsRollingUniqueCount
  };
  //social
  tabsData.socialreferringsites = {
    SocialSources: (function() {
      var oss = [];
      $.each(jsonData.TopSocial, function(i, os) {
        oss.push({
          Source: os.Name,
          Value: os.Value,
          Icon: os.Icon,
          Domain: os.site
        });
      });
      return oss;
    })()
  };
//mobile App
  if(jsonData.MobileApps) {
    tabsData.androidApp = {};
    tabsData.androidApp.RelatedApps = [];
    tabsData.iosApp = {};
    tabsData.iosApp.RelatedApps = [];
    var apps = jsonData.MobileApps;
    for(var m in apps) {
      var appData = {
        AppId: apps[m].AppId,
        Author: apps[m].Author,
        MainCategory: apps[m].Category,
        MainCategoryId: apps[m].Category.toLowerCase(),
        Cover: apps[m].Cover.indexOf("http") !== 0 ? "http:" + apps[m].Cover : apps[m].Cover,
        Price: apps[m].Price,
        Rating: apps[m].Rating,
        RatingCount: apps[m].RatingCount,
        Valid: apps[m].Valid,
        Title: apps[m].Title
      };
      if(apps[m].Key == 0) {
        tabsData.androidApp.RelatedApps.push(appData);
      } else {
        tabsData.iosApp.RelatedApps.push(appData);
      }
    }
  }
  //also visit
  tabsData.alsoVisted = {
    uniCount: jsonData.AlsoVisitedUniqueCount,
    Data: jsonData.TopAlsoVisited,
    ScreenShot: jsonData.LargeScreenshot

  };
  tabsData.adsNetWork = {
    //uniCount: jsonData.IncomingAdsRollingUniqueCount,
    Data: (function() {
      var oss = [];
      $.each(jsonData.TopAdNetworks, function(i, os) {
        oss.push({
          Site: os.Site,
          Display: os.Value
        });
      });
      return oss;
    })()
  };
  tabsData.adsPublisher = {
    uniCount: jsonData.IncomingAdsRollingUniqueCount,
    Data: (function() {
      var oss = [];
      $.each(jsonData.TopPublishers, function(i, os) {
        oss.push({
          Site: os.Site,
          Display: os.Value
        });
      });
      return oss;
    })()
   };

 
  }else{
    
  $("#tab-geo").addClass("tab-disabled");
  $('#tab-geo-content').find('.error_page').css('display', 'block');
  $('#tab-geo-content').find('.data-container').css('display', 'none');


  $("#tab-social").addClass("tab-disabled");
  $('#tab-social-content').find('.error_page').css('display', 'block');
  $('#tab-social-content').find('.data-container').css('display', 'none');
     // small site
  tabsData.SmallSite = {
    IsSmall: true,
    SmallSiteTitle:"",
    SmallSiteMessage: "no"
  }

  //overview
  tabsData.rankOverView = {
    //FavIcon: xml.find,
    Category: "Site Rank",
    CategoryRank: "not",
    Country: "jsonData.CountryRank.Country",
    CountryRank: "jsonData.CountryRank.Rank",
    GlobalRank: "jsonData.GlobalRank.Rank",
    //LastAvgTrafficReach:?
    SimilarSites: "jsonData.SimilarSites",
    Title: "jsonData.Title",
    //TrafficReach:?
    TrafficShares: "jsonData.TrafficSources",
    IsSiteVerified: false
  };

  tabsData.visits = {
    Values: (function() {
      var ts = [];
     
      return ts;
    })(),
    LatestPeriod: Date()
  };
  tabsData.visitduration = {
    Values: [{
      Date: Date(),
      Value: "jsonData.Engagments.TimeOnSite"
    }]
  };
  tabsData.pageviews = {
    Values: [{
      Date: Date(),
      Value: "jsonData.Engagments.PagePerVisit"
    }]
  };

  tabsData.bouncerate = {
    Values: [
      {
        Value: "jsonData.Engagments.BounceRate",
        Visits: "jsonData.Engagments.Visits"
      }
    ]
  };
  tabsData.socialreferringsites = {
    SocialSources:'true'
  };



  }
 
  return tabsData;
  
}
function renderingTab (forceReload, tabId) {

  var tabs = $('#tabs').find("ul li");

  loadDataWithNewAPI(forceReload, function(data) {
   // swpopup.domain = data.SiteName;
    data = parseNewJsonData(data);
    if(tabId) {
      load_tab(tabId, data);
    } else {
      $.each(tabs, function(i, tab) {  
        tab.onclick = function(e) {
          var tid = $(tab).attr("id");
          if(tid === "tab-ads") {
            localStorage.showAdNew = "0";
          }else if (tid === "tab-bestselling"){
            window.open("https://app.commercedetective.com/home/bestselling?url="+swpopup.current_site,'');
          }else if (tid === "tab-pro"){
            window.open("https://app.commercedetective.com/home?extentionuseragent="+localStorage.getItem('ExtentionuserAgent'));
          }
          $(tab).find(".new").hide();
          activate_tab(tid);
        };
        if(data){
          load_tab($(tab).attr("id"), data);
        }
        
      });
    }
  });
}